import React from 'react';
import { Field, reduxForm } from 'redux-form';

import {
  Form,
  FormGroup,
  Col,
  ControlLabel,
  Button,
  ButtonToolbar,
  HelpBlock,
} from 'react-bootstrap';

const validate = values => {
  const errors = {}
  if (!values.username) {
    errors.username = 'Required'
  } else if (values.username.length > 15) {
    errors.username = 'Must be 15 characters or less'
  }
  if (!values.email) {
    errors.email = 'Required'
  } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
    errors.email = 'Invalid email address'
  }
  if (!values.age) {
    errors.age = 'Required'
  } else if (isNaN(Number(values.age))) {
    errors.age = 'Must be a number'
  } else if (Number(values.age) < 18) {
    errors.age = 'Sorry, you must be at least 18 years old'
  }
  return errors
}

const MyForm = props => {
  const { handleSubmit, pristine, reset, submitting } = props;
  return (
    <Form horizontal onSubmit={handleSubmit}>
      <FormGroup controlId={'name'}>
        <Col componentClass={ControlLabel} sm={2}>お名前</Col>
        <Col sm={8}>
          <Field
            name="name"
            component={renderField}
            type="text"
            label="お名前"
            placeholder="Name"
          />
        </Col>
      </FormGroup>
      <FormGroup>
        <Col smOffset={2} sm={5}>
          <ButtonToolbar>
            <Button bsStyle={'primary'} type="submit" disabled={pristine || submitting}>登録</Button>
            <Button type="button" disabled={pristine || submitting} onClick={reset}>クリア</Button>
          </ButtonToolbar>
        </Col>
      </FormGroup>
    </Form>
  );
};

const renderField =
  ({
     input,
     label,
     type,
     placeholder,
     meta: {touched, error, warning}
   }) => {
    const validationState = error ? 'error' : warning ? 'warning' : 'success';
    return (
      <FormGroup controlId={input.name} validationState={touched ? validationState : null}>
        <Col componentClass={ControlLabel} sm={2}>{label}</Col>
        <Col sm={5}>
          <input {...input} id={input.name} placeholder={placeholder} type={type} className={'form-control'}/>
          {
            touched && error &&
            <HelpBlock>{error}</HelpBlock>
          }
        </Col>
      </FormGroup>
    )
  };

export default reduxForm({
  form: 'myFormA',
  validate
})(MyForm);
